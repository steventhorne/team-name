#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

int number;

int main (int argc, char **argv)
{
   number = 0;

   int pid = 1;

   int i = 0;
   do
   {
      if (pid == 0) break;

      if (pid != 0)
         pid = fork();

      i++;
   } while (i < 5);

   if (pid == 0)
   {
      while (number < 100)
      {
         number++;
         printf("pid: %d num: %d\n", getpid(), number);
      }
   }
   else
   {
      int status;
      while (-1 == waitpid(pid, &status, 0));
   }

   return 0;
}
