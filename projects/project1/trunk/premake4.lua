solution "Project1"
   configurations { "Debug", "Release" }

   project "Part1"
      kind "ConsoleApp"
      language "C"
      objdir "../obj/part1"
      location "pa1_part1"
      files { "pa1_part1/**.*" }
      targetname "pa1_part1"
      targetdir "../bin/"

   project "Part2"
      kind "ConsoleApp"
      language "C"
      objdir "../obj/part2"
      location "pa1_part2"
      files { "pa1_part2/**.*" }
      targetname "pa1_part2"
      targetdir "../bin/"

   project "Part3"
      kind "ConsoleApp"
      language "C"
      objdir "../obj/part3"
      location "pa1_part3"
      files { "pa1_part3/**.*" }
      targetname "pa1_part3"
      targetdir "../bin/"

      configurations "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }

      configurations "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }
