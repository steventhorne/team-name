#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX 100
#define FALSE 0
#define TRUE 1

int number;

int main (int argc, char **argv)
{
	/*
	FILE *rd_stream, *wr_stream;
	char buf[MAX];

	printf("Creating Pipe...Waiting for receiver process...\n\n");

	if(mkfifo("FIFO_PIPE", 0777) < 0)
	{
		perror("FIFO (named pipe) could not be created.");
		exit(-1);
	}
	else
		printf("\nPipe has been created...\n");

	if((wr_stream = fopen("FIFO_PIPE", "w")) < 0)
	{
		perror("Could not open named pipe.");
		exit(-1);
	}
	else
		printf("\nPipe has been opened\n");

	printf("\nCopying String\n");
	strcpy(buf, "Named Pipe experiment.");

	fwrite("Named Pipe experiment", 1, MAX, wr_stream);


	int pid = fork();

	if(pid == 0)//child process
	{
		printf("\n I am the child \n");

		if((rd_stream = fopen("FIFO_PIPE", "r")) < 0)
		{
			perror("CNONPFR");
			exit(-1);
		}
		printf("Pipe open try read\n\n");

		fread(buf, sizeof(char), MAX, rd_stream);

		printf("\nMessage i got pipe - %s\n", buf);


		if(unlink("FIFO_PIPE") < 0)
		{
			perror("edpf");
			exit(-1);
		}
	}
	else//parent process
	{
		printf("\n I am the parent \n");
	}
	*/

	int unnamedPipe[2];

	if(pipe(unnamedPipe) == -1)
	{
		perror("Error openning pipe.");
		exit(1);
	}
	else
	{
		printf("Pipe created\n");
	}

	number = 0;
	int pipeBeingUsed = FALSE;
	int pid = 1;
	int i = 0;

	do
	{
		if (pid == 0) break;

		if (pid != 0)
			pid = fork();

		i++;
	} while (i < 5);

	if (pid == 0)
	{
		while(number < 100)
		{
			printf("pid: %d Beginning loop\n", getpid());
			read(unnamedPipe[0], &pipeBeingUsed, MAX); 
			printf("pid: %d Finished reading pipe\n", getpid());
			if(pipeBeingUsed == FALSE)
			{
				printf("pid: %d Taking control of pipe\n", getpid());
				pipeBeingUsed = TRUE;
				write(unnamedPipe[0], &pipeBeingUsed, MAX);

				read(unnamedPipe[1], &number, MAX); 

				if(number >= 100)
				{
					printf("pid: %d Killing Process\n", getpid());
					pipeBeingUsed = FALSE;
					write(unnamedPipe[0], &pipeBeingUsed, MAX);
					return 0;
				}
				else
				{
					number = number + 1;
					printf("pid: %d num: %d\n", getpid(), number);
					write(unnamedPipe[1], &number, MAX);
				}

				printf("pid: %d Loop complete\n", getpid());
				pipeBeingUsed = FALSE;
				write(unnamedPipe[0], &pipeBeingUsed, MAX);
		}
		}
	}
	else
	{
		int status;
		while (-1 == waitpid(pid, &status, 0));
	}
	return 0;
}
