#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/ipc.h>

#define SHMKEY 666
#define LOCKKEY 667
#define SHMSIZE 1024

int number;

int main (int argc, char **argv)
{
   number = 0;

   int pid = 1;

   int shmid = 0;
   int shmid_lock = 0;
   int lockNum = 0;


   void *mempointer = NULL;
   void *lock_id = NULL;

   if ((shmid = shmget(SHMKEY, SHMSIZE, 0666 | IPC_CREAT)) < 0)
      perror("shmget");

   if ((shmid_lock = shmget(LOCKKEY, SHMSIZE, 0666 | IPC_CREAT)) < 0)
      perror("shmget");

   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   if ((lock_id = shmat(shmid_lock, NULL, 0)) == NULL)
      perror("shmat");

   memcpy(mempointer, &number, sizeof(number));
   memcpy(lock_id, &lockNum, sizeof(lockNum));

   int i = 0;
   do
   {
      if (pid == 0) break;

      if (pid != 0)
         pid = fork();

      i++;
   } while (i < 5);

   if (pid == 0)
   {
	   //Child Process here
      if ((shmid = shmget(SHMKEY, SHMSIZE, 0444)) < 0)
         perror("shmget");

      if ((shmid_lock = shmget(LOCKKEY, SHMSIZE, 0444)) < 0)
         perror("shmget");

      int num = 0;
      int id = 0;

      if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      {
         perror("shmat");
      }

      if ((lock_id = shmat(shmid_lock, NULL, 0)) == NULL)
         perror("shmat");

      unsigned char *uc_address = (unsigned char *)lock_id;
         id = (
            (uc_address[3] << 24) |
            (uc_address[2] << 16) |
            (uc_address[1] << 8) |
            uc_address[0]
            );

      int newId = id + 1;

      if (newId >= 5) newId = 0;

      memcpy(lock_id, &(newId), sizeof(id));

      do
      {
         uc_address = (unsigned char *)lock_id;
         if ((newId = (
            (uc_address[3] << 24) |
            (uc_address[2] << 16) |
            (uc_address[1] << 8) |
            uc_address[0]
            )) != id)
         {
            //fprintf(stderr, "%d\n",newId);
            continue;
         }
         else
         {
            newId++;
            if (newId >= 5) newId = 0;
            memcpy(lock_id, &newId, sizeof(newId));
         }

         uc_address = (unsigned char *)mempointer;
         num = (
            (uc_address[3] << 24) |
            (uc_address[2] << 16) |
            (uc_address[1] << 8) |
            uc_address[0]
            );

         //num = *((unsigned int*)mempointer);

         num++;

         if (num <= 100)
            fprintf(stderr, "%d: %d\n", getpid(), num);

         memcpy(mempointer, &num, sizeof(num));

      } while (num < 100);
   }
   else
   {
      int status;
      while (-1 == waitpid(pid, &status, 0));
   }

   if (shmdt(mempointer) < 0)
      perror("shmdt");

   if (shmdt(lock_id) < 0)
      perror("shmdt");

   return 0;
}
