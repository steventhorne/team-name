#include "functions.h"

int main()
{
	InitProcesses();
	runFIFO();
   runShortestTaskFirst();
   runPriorityQueue();
   runRoundRobin();
   runRoundRobinPriority();
	return 0;
}
