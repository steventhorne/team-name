#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void InitProcesses()
{
	srand(time(NULL));

	double randomNumber = 0;
	int i = 0;
	for( i =0; i < NUM_PROCESSES; i++)
	{
		randomNumber = (rand() % MAX_PROCESS_TIME) + 1;
		mProcesses[i].ProcessTime = randomNumber;
      mProcesses[i].TimeLeft = randomNumber;

		randomNumber = (rand() % 3) + 1;
		mProcesses[i].Priority = randomNumber;

		mProcesses[i].ProcessID = i+1;
	}
}

void runFIFO()
{
   runProcesses(mProcesses);
}

void runShortestTaskFirst()
{
   Process newList[NUM_PROCESSES];
   int i;
   for (i = 0; i < NUM_PROCESSES; i++)
   {
      newList[i] = mProcesses[i];
   }
   bubbleSortTime(newList);

   runProcesses(newList);
}

void runPriorityQueue()
{
   Process newList[NUM_PROCESSES];
   int i;
   for (i = 0; i < NUM_PROCESSES; i++)
   {
      newList[i] = mProcesses[i];
   }

   bubbleSortPriority(newList);

   runProcesses(newList);
}

void runRoundRobin()
{
   Process processes[NUM_PROCESSES];
   int i;
   for (i = 0; i < NUM_PROCESSES; i++)
   {
      processes[i] = mProcesses[i];
   }

   runRoundProcesses(processes);
}

void runRoundRobinPriority()
{
   Process processes[NUM_PROCESSES];
   int i;
   for (i = 0; i < NUM_PROCESSES; i++)
   {
      processes[i] = mProcesses[i];
   }

   bubbleSortPriority(processes);

   runRoundProcesses(processes);
}

void bubbleSortTime(Process newProcs[])
{
  int i, j;
  Process temp;

  for (i = (NUM_PROCESSES - 1); i > 0; i--)
  {
    for (j = 1; j <= i; j++)
    {
      if (newProcs[j-1].ProcessTime > newProcs[j].ProcessTime)
      {
        temp = newProcs[j-1];
        newProcs[j-1] = newProcs[j];
        newProcs[j] = temp;
      }
    }
  }
}

void bubbleSortPriority(Process newProcs[])
{
  int i, j;
  Process temp;

  for (i = (NUM_PROCESSES - 1); i > 0; i--)
  {
    for (j = 1; j <= i; j++)
    {
      if (newProcs[j-1].Priority > newProcs[j].Priority)
      {
        temp = newProcs[j-1];
        newProcs[j-1] = newProcs[j];
        newProcs[j] = temp;
      }
    }
  }
}

void runProcesses(Process processes[])
{
   double startTime[NUM_PROCESSES];
	double endTime[NUM_PROCESSES];

	double currentStartTime = 0;
	double currentEndTime = 0;

	int i;
	for(i = 0; i < NUM_PROCESSES; i++)
	{
		startTime[i] = currentStartTime;

		currentStartTime += processes[i].ProcessTime;

		currentEndTime = currentStartTime;
		endTime[i] = currentEndTime;
	}

	double averageWaitTime = 0;
	double averageTurnaroundTime = 0;
	for(i = 0; i < NUM_PROCESSES; i++)
	{
      fprintf(stderr, "PID: %02d     Priority: %d     Time: %05.2f     Start: %06.2f     End: %06.2f\n", processes[i].ProcessID, processes[i].Priority, processes[i].ProcessTime, startTime[i], endTime[i]);
		averageWaitTime += startTime[i];
		averageTurnaroundTime += endTime[i];
	}

	averageWaitTime /= NUM_PROCESSES;
	averageTurnaroundTime /= NUM_PROCESSES;

	fprintf(stderr, "Average wait time was %.2f\n", averageWaitTime);
	fprintf(stderr, "Average turn around time was %.2f\n", averageTurnaroundTime);
}

void runRoundProcesses(Process processes[])
{
   double currentStartTime = 0;
   double currentEndTime = 0;

   double averageWaitTime = 0;
   double averageTurnaroundTime = 0;

   int count = 0;

   while (hasTimeLeft(processes))
   {
      int i;
      for(i = 0; i < NUM_PROCESSES; i++)
      {
         if (processes[i].TimeLeft <= 0)
            continue;

         count++;

         double tempStartTime = currentStartTime;

         if (processes[i].TimeLeft <= ROUND_PROCESS_TIME)
         {
            currentStartTime += processes[i].TimeLeft;
            processes[i].TimeLeft = 0;
         }
         else
         {
            currentStartTime += ROUND_PROCESS_TIME;
            processes[i].TimeLeft -= ROUND_PROCESS_TIME;
         }

         currentEndTime = currentStartTime;

         averageWaitTime += tempStartTime;
         averageTurnaroundTime += currentEndTime;

         fprintf(stderr, "PID: %02d     Priority: %d     Time: %05.2f     Remaining: %05.2f     Start: %06.2f     End: %06.2f\n", processes[i].ProcessID, processes[i].Priority, processes[i].ProcessTime, processes[i].TimeLeft, tempStartTime, currentEndTime);
      }
   }

   averageWaitTime /= count;
   averageTurnaroundTime /= count;

   fprintf(stderr, "Average wait time was %.2f\n", averageWaitTime);
   fprintf(stderr, "Average turn around time was %.2f\n", averageTurnaroundTime);
}

bool hasTimeLeft(Process processes[])
{
   int i;
   for (i = 0; i < NUM_PROCESSES; i++)
   {
      if (processes[i].TimeLeft > 0)
         return true;
   }

   return false;
}
