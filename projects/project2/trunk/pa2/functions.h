#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define MAX_PROCESS_TIME 60
#define ROUND_PROCESS_TIME 20
#define NUM_PROCESSES 10

#define true 1
#define false 0
typedef int bool;

typedef struct
{
	int ProcessID;
	int Priority;
	double ProcessTime;
   double TimeLeft;

}Process;


Process mProcesses[NUM_PROCESSES];

void InitProcesses();
void runFIFO();
void runShortestTaskFirst();
void runPriorityQueue();
void runRoundRobin();
void runRoundRobinPriority();

void bubbleSortTime(Process newProcs[]);
void bubbleSortPriority(Process newProcs[]);

void runProcesses(Process processes[]);
void runRoundProcesses(Process processes[]);

bool hasTimeLeft();

#endif
