solution "Project2"
   configurations { "Debug", "Release" }

   project "PA2"
      kind "ConsoleApp"
      language "C"
      objdir "../obj/pa2"
      location "pa2/"
      files { "pa2/**.*" }
      targetname "pa2"
      targetdir "../bin"
