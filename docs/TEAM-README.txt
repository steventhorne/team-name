Things you'll need:

premake4 4.4 beta: http://industriousone.com/premake/download

make for windows: http://gnuwin32.sourceforge.net/packages/make.htm

premake4 info:
used to create the makefiles for us. modify the premake4.lua script to change how the makefiles will be generated.
Pretty easy to figure out and use. You only need to modify the .lua script when in certain circumstances, not often, and definitely not every time you run make.
If you change the .lua file simply run "premake4 gmake" to regenerate the makefiles.
