#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

typedef unsigned char byte;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
    char name[9];
    char extension[4];
    int starting_byte;
    unsigned char attributes;
    short creation_time;
    short creation_date;
    short last_access_date;
    short last_write_time;
    short last_write_date;
    short flc;
    int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);
extern void writeSection( int startingByte, int sectorSize, unsigned char* boot, unsigned char* data);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern unsigned int get_fat_entry(int fat_entry_number, char* fat);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void tryToCreate(unsigned char* boot, char* filePath, int flc)
{
    char* fileName;
    memcpy(fileName, filePath, strlen(filePath) + 1);
    printf( "");//DO NOT REMOVE! WILL BREAK OS

    FILE_INFO info = get_file_info(filePath, boot, flc);

    char EmptyChars[] = "";
    char temp;
    int startingByte = -1;
    int openFLC = 0;
    if (info.flc == -1)
    {
        int i, j;

        for(i = 0; i < 16; ++i)
        {
            /************************************************************
             *  File Name         Staring Byte: 0      Length: 8 Bytes   *
             *************************************************************/
            for (j = 0; j < 8; j++)
            {
                temp = readSection(i*32 + j, 1, boot);
                info.name[j] = temp;
            }
            info.flc = readSection(i*32 + 26, 2, boot);
            info.name[8] = '\0';
            if(strcmp(info.name, EmptyChars) == 0)
            {
                startingByte = i * 32;

                break;
            }

        }


        for(i = 0; i < 16; ++i)
        {
            if(get_fat_entry(31 + i, boot) == 0x000)
            {
                openFLC = i;
                break;
            }
        }

        if(startingByte == -1 || openFLC == -1)
        {
            fprintf(stderr, "Error:No Available Space In Directory\n");
            return;
        }
        else
        {

            char* slashSaveToken, dotSaveToken;
            char* test;


            test = strtok_r( fileName, "/" , &slashSaveToken);
            while(fileName != NULL)
            {
                if(fileName != NULL)
                {
                    memcpy(test, fileName, strlen(fileName) + 1);
                }
                fileName = strtok_r(NULL, "/", &slashSaveToken);
            }

            char* extension = strtok_r(test, ".", &dotSaveToken);
            extension = strtok_r(NULL, ".", &dotSaveToken);

            int realLen;
            char realExt[4];

            if(extension == NULL)
            {
                realLen = 0;
            }
            else if(strlen(extension) > 3)
            {
                realLen = 3;
            }
            else
            {
                realLen = strlen(extension);
            }

            for(i = 0; i < realLen; ++i)
            {
                realExt[i] = extension[i];
            }
            for(i = realLen; i < 3; ++i)
            {
                realExt[i] = ' ';
            }
            realExt[3] = '\0';

            char realDir[4];

            if(strlen(filePath) > 8)
            {
                realLen = 8;
            }
            else
            {
                realLen = strlen(filePath);
            }

            for(i = 0; i < realLen; ++i)
            {
                realDir[i] = filePath[i];
            }
            for(i = realLen; i < 8; ++i)
            {
                realDir[i] = ' ';
            }
            realDir[8] = '\0';

            unsigned char FLC[2], ATT[1];


            FLC[0] = (unsigned char)openFLC & 0xffffffff;
            FLC[1] = ((unsigned char)openFLC << 8) & 0xffffffff;

            ATT[0] = (unsigned char) 0x10 & 0xffffffff;

            writeSection(startingByte, 8, boot,realDir);            //File Name
            writeSection(startingByte + 8, 3, boot, realExt);    //Extension
            writeSection(startingByte + 26, 2, boot, FLC);       // FLC
            writeSection(startingByte + 11, 1, boot, ATT);      //Attributes
            set_fat_entry(openFLC + 31, 0xFFF, boot);
            write_sector(flc, boot);
            read_sector(flc, boot);
            get_fat_entry(openFLC + 31, boot);

            read_sector(openFLC, boot);
            writeSection(0, 8, boot, ".       ");
            writeSection(8, 3, boot, "   ");
            writeSection(26, 2, boot, openFLC);
            writeSection(11, 1, boot, ATT);
            writeSection(32, 8, boot, "..      ");
            writeSection(40, 3, boot, "   ");
            writeSection(58, 2, boot, flc);
            writeSection(43, 1, boot, ATT);
            write_sector(openFLC, boot);
            read_sector(openFLC, boot);
        }
    }
    else
    {
        fprintf(stderr, "Error: There is already a file with that name\n");
    }
}

int main( int argc, char **argv )
{
    int shmid = 0;
    void *mempointer = NULL;
    //find shm segment
    if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
        perror("shmget");

    //allocate shm to address space
    if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
        perror("shmat");

    char* floppyImageName;
    char* currentFLCStr;
    int currentFLC;

    int wordCount = 0;
    char* nextWordPtr = strtok(mempointer, "~");
    floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    floppyImageName[strlen(floppyImageName)] = '\0';

    nextWordPtr = strtok(NULL, "~");
    FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    FILE_PATH[strlen(FILE_PATH)] = '\0';

    nextWordPtr = strtok(NULL, "~");
    currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    currentFLCStr[strlen(currentFLCStr)] = '\0';

    sscanf(currentFLCStr, "%d", &currentFLC);

    FILE_SYSTEM_ID = fopen(mempointer, "r+");

    BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
    unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));

    tryToCreate(boot, argv[1], currentFLC);

    //deallocate shm
    if (shmdt(mempointer) < 0)
        perror("shmdt");
    return 0;
}
