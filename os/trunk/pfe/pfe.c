#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdlib.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_PER_SECTOR 512

FILE* FILE_SYSTEM_ID;
int BYTES_PER_SECTOR;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);

extern int  get_fat_entry(int fat_entry_number, char* fat);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);

int readFAT12Table(int num, unsigned char* boot)
{
   int sector = get_fat_entry(num, (char *) boot);
   return (int) sector;
}

int main(int argc, char **argv)
{
   int i;
   BYTES_PER_SECTOR = BYTES_TO_READ_PER_SECTOR;

   int x;
   int y;

   int error = 0;

   int shmid = 0;
   void *mempointer = NULL;

   //find shm segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0444)) < 0)
      perror("shmget");

   //allocate shm to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   //if (mempointer == NULL)
      //executed from commandline

   sscanf(argv[1], "%d", &x);
   sscanf(argv[2], "%d", &y);

   //check for errors
   if (x > y)
   {
      printf("Error: arg1 cannot be greater than arg2.\n");
      error = 1;
   }
   if (x < 2)
   {
      printf("Error: arg1 cannot be less than 2.\n");
      error = 1;
   }
   if (error)
      return 1;

   FILE_SYSTEM_ID = fopen(mempointer, "r+");

   unsigned char* boot = (unsigned char*) malloc(BYTES_PER_SECTOR * sizeof(unsigned char));

   for (i = 0; i < 18; i++)
   {
      if (read_sector(i+1, (char *)&boot[i*BYTES_PER_SECTOR]) == -1)
         printf("Something has gone wrong -- could not read the boot sector!\n");
   }

   for (i = x + 1; i <= y; i++)
   {
      int toDisplay = readFAT12Table(i, boot);
      printf("%d: %X\n", i, toDisplay);
   }

   //deallocate shm
   if (shmdt(mempointer) < 0)
      perror("shmdt");

   return 0;
}
