#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

typedef unsigned char byte;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
   char name[9];
   char extension[4];
   int starting_byte;
   unsigned char attributes;
   short creation_time;
   short creation_date;
   short last_access_date;
   short last_write_time;
   short last_write_date;
   short flc;
   int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void setFilePath(char* original, char* new)
{
   if (original[strlen(original)-1] != '/' && new[0] != '/')
      strcat(original, "/");
   strcat(original, new);
}

/********************************************************
*  Returns the flc of the current directory
*  Returns -1 if the command is not a directory
*********************************************************/
int findNewFLC(unsigned char* boot, char* dirname, int flc)
{
   if (strcmp(dirname, "/") == 0)
   {
      memcpy(FILE_PATH, "/", strlen("/") + 1);
      return 19;
   }
   
   char* newDirName = malloc(strlen(dirname) + 1);
   strcpy(newDirName, dirname);
   FILE_INFO info = get_file_info(newDirName, boot, flc);
   
   if (info.flc == -1)
   {
      fprintf(stderr, "%s not found\n", info.name);
      
      return flc;
   }
   else
   {
      int attCheck = 0x10;
      if ((info.attributes & attCheck) && attCheck)
      {
         if (info.flc != 0)
         {
            if (dirname[0] == '/')
            {
               memcpy(FILE_PATH, "", strlen("") + 1);
               setFilePath(FILE_PATH, dirname);
            }
            else
               setFilePath(FILE_PATH, dirname);
            return info.flc + 31;
         }
         
         memcpy(FILE_PATH, "/", strlen("/")+1);
         return 19;
      }
      else
      {
         fprintf(stderr, "That is not a directory.\n");
         return flc;
      }
   }
   /*read_sector(flc, (char *)boot );

   char temp;
   int i = 0;
   FILE_NAME[8] = '\0';

   int j;
   for (j = 0; j < 16; j++)
   {
      for (i = 0; i < 8; i++)
      {
         temp = readSection(j*32 + i, 1, boot);
         FILE_NAME[i] = temp;
      }

      if (strcasecmp(FILE_NAME, dirname) == 0)
      {
         //does exist
         byte att = readSection(j*32 + 11, 1, boot);

         if ((att & (byte)0x10) && 0x10)
         {
            //is a dir
            int num = readSection(j*32 + 26, 2, boot);
            char temp[255];
            strcpy(temp, "");
            strcat(temp, FILE_PATH);
            strcat(temp, FILE_NAME);
            strcat(temp, "/");

            memcpy(FILE_PATH, temp, strlen(temp) + 1);
            if (num != 0)
               return num + 31;

            return 19;
         }
      }
   }

   printf("That is not a directory.\n");
   return -1;*/
}

int main( int argc, char **argv )
{
   int shmid = 0;
   void *mempointer = NULL;
   //find shm segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
      perror("shmget");

   //allocate shm to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   char newDir[9];


   char* floppyImageName;
   char* currentFLCStr;
   int currentFLC;

   int wordCount = 0;
   char* nextWordPtr = strtok(mempointer, "~");
   floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   floppyImageName[strlen(floppyImageName)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   FILE_PATH[strlen(FILE_PATH)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   currentFLCStr[strlen(currentFLCStr)] = '\0';

   sscanf(currentFLCStr, "%d", &currentFLC);

   char newSHM[255];

   if(argc == 1)
   {

      newDir[0] = '/';
      newDir[1] = ' ';
      newDir[2] = ' ';
      newDir[3] = ' ';
      newDir[4] = ' ';
      newDir[5] = ' ';
      newDir[6] = ' ';
      newDir[7] = ' ';
      newDir[8] = '\0';

      currentFLC = 19;

      strcpy(newSHM, "/~19");
      memcpy(mempointer, newSHM, strlen(newSHM) + 1);
   }
   else if(argc == 2)
   {

      newDir[0] = ' ';
      newDir[1] = ' ';
      newDir[2] = ' ';
      newDir[3] = ' ';
      newDir[4] = ' ';
      newDir[5] = ' ';
      newDir[6] = ' ';
      newDir[7] = ' ';
      newDir[8] = '\0';

      int i;
      int count = strlen(argv[1]);

      if( count > 8)
      {
         count = 8;
      }
      for(i = 0; i < count; i++)
      {
         if(argv[1][i] == (int)NULL)
            newDir[i] = ' ';
         else
            newDir[i] = argv[1][i];
      }

      FILE_SYSTEM_ID = fopen(mempointer, "r+");

      BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
      unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));

      currentFLC = findNewFLC(boot, argv[1], currentFLC);



      if(currentFLC == -1)
      {
         strcpy(newSHM, "");
         strcat(newSHM, FILE_PATH);
         strcat(newSHM, "~");
         strcat(newSHM, currentFLCStr);

         memcpy(mempointer, newSHM, strlen(newSHM) + 1);
      }
      else
      {
         strcpy(newSHM, "");
         strcat(newSHM, FILE_PATH);
         strcat(newSHM, "~");
         char temp[255];
         snprintf(temp, 255, "%d", currentFLC);
         memcpy(currentFLCStr, temp, strlen(temp) + 1);
         strcat(newSHM, currentFLCStr);
         memcpy(mempointer, newSHM, strlen(newSHM) + 1);
         //fprintf(stderr, "NewSHM: %s\n", newSHM);
      }
   }





   //deallocate shm
   if (shmdt(mempointer) < 0)
      perror("shmdt");
   return 0;
}
