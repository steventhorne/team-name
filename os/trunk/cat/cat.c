#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

typedef unsigned char byte;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
    char name[9];
    char extension[4];
    int starting_byte;
    unsigned char attributes;
    short creation_time;
    short creation_date;
    short last_access_date;
    short last_write_time;
    short last_write_date;
    short flc;
    int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);
extern void writeSection( int startingByte, int sectorSize, unsigned char* boot, unsigned char* data);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void tryToDelete(unsigned char* boot, char* filePath, int flc)
{
    printf( "");//DO NOT REMOVE! WILL BREAK OS
    
    FILE_INFO info = get_file_info(filePath, boot, flc);
    
    if (info.flc == -1)
    {
        fprintf(stderr, "%s:%s not found\n", info.name, filePath);
    }
    else
    {
        fprintf(stderr, "1\n");
        int checkAtt = 0x10;
        if (((info.attributes & checkAtt) && checkAtt))
        {
            fprintf(stderr, "%s is a directory.\n", info.name);
            return;
        }
        
        fprintf(stderr, "2\n");
        
        int fileSize = info.file_size;
        
        if (fileSize > 511)
        {
            fileSize = 511;
        }
        
        char* data = (char*)malloc(fileSize + 1);
        
        fprintf(stderr, "3\n");
        
        int currentFlc = 0;
        
        if (info.flc != 0)
            currentFlc = info.flc + 31;
        else
            currentFlc = 19;
        
        fprintf(stderr, "4\n");
        
        read_sector(currentFlc, boot);
        
        fprintf(stderr, "5\n");
        
        int i;
        for(i = 0; i < fileSize; ++i)
        {
            fprintf(stderr, "%s", readSection(i, 1, boot));
        }
        
        //fprintf(stderr, "%s\n", data);
        
        read_sector(flc, boot); //reset
    }
}

int main( int argc, char **argv )
{
    int shmid = 0;
    void *mempointer = NULL;
    //find shm segment
    if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
        perror("shmget");
    
    //allocate shm to address space
    if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
        perror("shmat");
    
    char* floppyImageName;
    char* currentFLCStr;
    int currentFLC;
    
    int wordCount = 0;
    char* nextWordPtr = strtok(mempointer, "~");
    floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    floppyImageName[strlen(floppyImageName)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    FILE_PATH[strlen(FILE_PATH)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    currentFLCStr[strlen(currentFLCStr)] = '\0';
    
    sscanf(currentFLCStr, "%d", &currentFLC);
    
    FILE_SYSTEM_ID = fopen(mempointer, "r+");
    
    BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
    unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));
    
    tryToDelete(boot, argv[1], currentFLC);
    
    //deallocate shm
    if (shmdt(mempointer) < 0)
        perror("shmdt");
    return 0;
}
