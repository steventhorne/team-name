#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

#define true 1;
#define false 0;

typedef unsigned char byte;
typedef int bool;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
    char name[9];
    char extension[4];
    int starting_byte;
    unsigned char attributes;
    short creation_time;
    short creation_date;
    short last_access_date;
    short last_write_time;
    short last_write_date;
    short flc;
    int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);
extern void writeSection( int startingByte, int sectorSize, unsigned char* boot, unsigned char* data);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void tryToDelete(unsigned char* boot, char* filePath, int flc)
{
    int currentFlc = 0;
    
    printf( "");//DO NOT REMOVE! WILL BREAK OS
    FILE_INFO dirInfo = get_file_info(filePath, boot, flc);
    
    if (dirInfo.flc == -1)
    {
        fprintf(stderr, "%s does not exist.\n", dirInfo.name);
        return;
    }
    else
    {
        if (dirInfo.flc != 0)
            currentFlc = dirInfo.flc + 31;
        else
            currentFlc = 19;
    }
    
    byte checkAtt;
    
    checkAtt = 0x10;
    if (!((dirInfo.attributes & checkAtt) && checkAtt))
    {
        fprintf(stderr, "%s is not a directory.\n", dirInfo.name);
        return;
    }
    
    read_sector(currentFlc, (char *)boot );
    
    bool empty = true;
    
    char temp;
    int i = 0;
    int firstChar = 0;
    
    char* FileName = (char*)malloc(sizeof(char)*9);
    FileName[8] = '\0';
    byte attributes;
    
    int j;
    for (j = 0; j < 16; j++)
    {
        char* strippedFileName;
        char* strippedExtension;
        bool hasExtension = false;
        bool newExt = false;
        bool newFile = false;
        
        /************************************************************
         *  File Name         Staring Byte: 0      Length: 8 Bytes   *
         *************************************************************/
        for (i = 0; i < 8; i++)
        {
            if (i == 0)
                firstChar = readSection(j*32 + i, 1, boot);
            
            temp = readSection(j*32 + i, 1, boot);
            FileName[i] = temp;
        }
        
        if (firstChar == 0xE5 || firstChar == 0x0 || firstChar == 0x2E)
        {
            continue;
        }

        empty = false;
    }

    if (empty)
    {
        printf( "");//DO NOT REMOVE! WILL BREAK OS

        FILE_INFO info = get_file_info(filePath, boot, flc);
        
        if (info.flc == -1)
        {
            fprintf(stderr, "%s:%s not found\n", info.name, filePath);
        }
        else
        {
            int currentFlc = 0;
            
            if (info.flc != 0)
                currentFlc = info.flc + 31;
            else
                currentFlc = 19;
            
            
            unsigned char deleted = 0xE5;
            
            writeSection(info.starting_byte, 1, boot, &deleted);
            write_sector(flc, boot);
            
            set_fat_entry(currentFlc, 0x00, boot);
        }
    }
    else
    {
        fprintf(stderr, "%s is not empty.\n", dirInfo.name);
    }
}

int main( int argc, char **argv )
{
    int shmid = 0;
    void *mempointer = NULL;
    //find shm segment
    if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
        perror("shmget");
    
    //allocate shm to address space
    if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
        perror("shmat");
    
    char* floppyImageName;
    char* currentFLCStr;
    int currentFLC;
    
    int wordCount = 0;
    char* nextWordPtr = strtok(mempointer, "~");
    floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    floppyImageName[strlen(floppyImageName)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    FILE_PATH[strlen(FILE_PATH)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    currentFLCStr[strlen(currentFLCStr)] = '\0';
    
    sscanf(currentFLCStr, "%d", &currentFLC);
    
    FILE_SYSTEM_ID = fopen(mempointer, "r+");
    
    BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
    unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));
    
    tryToDelete(boot, argv[1], currentFLC);
    
    //deallocate shm
    if (shmdt(mempointer) < 0)
        perror("shmdt");
    return 0;
}
