#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 62

FILE* FILE_SYSTEM_ID;

int BYTES_PER_SECTOR;
int SECTORS_PER_CLUSTER;
int NUM_FATS;
int NUM_SECTORS;
int NUM_ROOT;
int TOTAL_SECTORS;
int SECTORS_PER_FAT;
int SECTORS_PER_TRACK;
int NUM_HEADS;
int BOOT_SIGNATURE;
int VOLUME_ID;
char VOLUME_LABEL[12];
char FILE_SYSTEM_TYPE[9];

extern int read_sector(int sector_number, char* buffer);
extern int readSection(int startingByte, int sectorSize, unsigned char* boot);

void readBootSector(unsigned char* boot)
{
   read_sector(0, (char *)boot );

   //Bytes per sector            11 - 12
   BYTES_PER_SECTOR = readSection(11, 2, boot);

   //Sectors per cluster         13
   SECTORS_PER_CLUSTER = readSection(13, 1, boot);

   //Number of FATS           16
   NUM_FATS = readSection(16, 1, boot);

   //Number of reserved sectors  14 - 15
   NUM_SECTORS = readSection(14, 2, boot);

   //Number of root entries      17 - 18
   NUM_ROOT = readSection(17, 2, boot);

   //Total sector count       19 - 20
   TOTAL_SECTORS = readSection(19, 2, boot);

   //Sectors per FAT          22 - 23
   SECTORS_PER_FAT = readSection(22, 2, boot);

   //Sectors per track           24 - 25
   SECTORS_PER_TRACK = readSection(24, 2, boot);

   //Number of heads          26 - 27
   NUM_HEADS = readSection(26, 2, boot);

   //Boot signature (in hex)     38
   BOOT_SIGNATURE = readSection(38, 1, boot);

   //Volume ID (in hex)       39 - 42
   VOLUME_ID = readSection(39, 4, boot);

   int i;
   for (i = 0; i < 11; ++i)
   {
      VOLUME_LABEL[i] = readSection(43+i, 1, boot);
   }


   for (i = 0; i < 8; ++i)
   {
      FILE_SYSTEM_TYPE[i] = readSection(54+i, 1, boot);
   }
}

void printBootSector()
{
   printf("Bytes per sector\t\t\t = %d\n", BYTES_PER_SECTOR);

   printf("Sectors per cluster\t\t\t = %d\n", SECTORS_PER_CLUSTER);

   printf("Number of FATS\t\t\t\t = %d\n", NUM_FATS);

   printf("Number of reserved sectors\t\t = %d\n", NUM_SECTORS);

   printf("Number of root entries\t\t\t = %d\n", NUM_ROOT);

   printf("Total sector count\t\t\t = %d\n", TOTAL_SECTORS);

   printf("Sectors per FAT\t\t\t\t = %d\n", SECTORS_PER_FAT);

   printf("Sectors per track\t\t\t = %d\n", SECTORS_PER_TRACK);

   printf("Number of heads\t\t\t\t = %d\n", NUM_HEADS);

   printf("Boot signature (in hex)\t\t\t = 0x%X\n", BOOT_SIGNATURE);

   printf("Volume ID (int hex)\t\t\t = 0x%X\n", VOLUME_ID);

   printf("Volume label\t\t\t\t = %s\n", VOLUME_LABEL);

   printf("File system type\t\t\t = %s\n", FILE_SYSTEM_TYPE);
}

int main( int argc, char **argv )
{

   int shmid = 0;
   void *mempointer = NULL;

   //find shm segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0444)) < 0)
      perror("shmget");

   //allocate shm to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   FILE_SYSTEM_ID = fopen(mempointer, "r+");

   BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
   unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));

   readBootSector(boot);
   printBootSector();

   //deallocate shm
   if (shmdt(mempointer) < 0)
      perror("shmdt");

   return 0;
}
