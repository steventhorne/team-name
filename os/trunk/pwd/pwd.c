#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

char FILE_NAME[9];

int main( int argc, char **argv )
{
	int shmid = 0;
	void *mempointer = NULL;
	//find shm segment
	if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
		perror("shmget");

	//allocate shm to address space
	if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
		perror("shmat");
	
   fprintf(stderr, "%s\n", mempointer);

	//deallocate shm
	if (shmdt(mempointer) < 0)
		perror("shmdt");
	return 0;
}

