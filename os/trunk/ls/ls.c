#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

#define true 1;
#define false 0;

typedef unsigned char byte;
typedef int bool;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
   char name[9];
   char extension[4];
   int starting_byte;
   unsigned char attributes;
   short creation_time;
   short creation_date;
   short last_access_date;
   short last_write_time;
   short last_write_date;
   short flc;
   int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

/********************************************************
*  Returns the flc of the current directory
*  Returns -1 if the command is not a directory
*********************************************************/

char* trimWhiteSpace(char *str)
{
   char *end;
   
   // Trim leading space
   while(isspace(*str)) str++;
   
   if(*str == 0)  // All spaces?
      return str;
   
   // Trim trailing space
   end = str + strlen(str) - 1;
   while(end > str && isspace(*end)) end--;
   
   // Write new null terminator
   *(end+1) = 0;
   
   return str;
}

void printFile(unsigned char* boot, int flc, char* name)
{
   read_sector(flc, (char *)boot);
   
   char* newDirName = malloc(strlen(name) + 1);
   strcpy(newDirName, name);
   FILE_INFO info = get_file_info(newDirName, boot, flc);
   
   if (info.flc == -1)
   {
      fprintf(stderr, "%s not found\n", info.name);
      
      return;
   }
   else
   {
      fprintf(stderr, "Name\t\tType\t\tFile Size\tFLC\n");
      int attCheck = 0x10;
      if (!((info.attributes & attCheck) && attCheck))
      {
         fprintf(stderr, "%s.%s\tFile\t\t%d\t\t%d\n", trimWhiteSpace(info.name), info.extension, info.file_size, info.flc);
      }
      else
      {
         fprintf(stderr, "%s\t\tDir\t\t%d\t\t%d\n", trimWhiteSpace(info.name), info.file_size, info.flc);
      }
   }
}

void printSector(unsigned char* boot, int flc)
{
   read_sector(flc, (char *)boot );

   char temp;
   int i = 0;
   int firstChar = 0;

   char* FileName = (char*)malloc(sizeof(char)*13);
   byte attributes;
   byte checkAtt;

   fprintf(stderr, "Name\t\tType\t\tFile Size\tFLC\n");
   
   int j;
   for (j = 0; j < 16; j++)
   {
      char* strippedFileName;
      char* strippedExtension;
      bool hasExtension = false;
      bool newExt = false;
      bool newFile = false;

      /************************************************************
      *  File Name         Staring Byte: 0      Length: 8 Bytes   *
      *************************************************************/
      for (i = 0; i < 8; i++)
      {
         if (i == 0)
            firstChar = readSection(j*32 + i, 1, boot);
         temp = readSection(j*32 + i, 1, boot);
         FileName[i] = temp;
      }
      if (firstChar == 0xE5 || firstChar == 0x0)
      {
         continue;
      }

      attributes = readSection(j*32 + 11, 1, boot);
      checkAtt = 0x10;
      if (!((attributes & checkAtt) && checkAtt))
      {
         FileName[i] = '.';

         /************************************************************
         *  Extension         Staring Byte: 8      Length: 3 Bytes   *
         *************************************************************/
         for (i = 8; i < 11; i++)
         {
            temp = readSection(j*32 + i, 1, boot);
            FileName[i + 1] = temp;
         }

         hasExtension = true;
      }
      else
      {
         FileName[8] = ' ';
         FileName[9] = ' ';
         FileName[10] = ' ';
         FileName[11] = ' ';
      }

      FileName[12] = '\0';
      FILE_INFO info = get_file_info(FileName, boot, flc);

      //If the file is hidden or archived continue
      checkAtt = 0x02;
      if (((info.attributes & checkAtt) && checkAtt))// || ((attributes & (byte)0x02) && 0x02))
      {
         continue;
      }

      /*
      *  Info is received
      **********************************/
      
      if (strcmp(info.extension, "   ") == 0)
      {
         fprintf(stderr, "%s\t\tDir\t\t%d\t\t%d\n", trimWhiteSpace(info.name), info.file_size, info.flc);
      }
      else
         fprintf(stderr, "%s.%s\tFile\t\t%d\t\t%d\n", trimWhiteSpace(info.name), info.extension, info.file_size, info.flc);
      /*
      for (i = 0; i < 8; i++)
      {
         fprintf(stderr, "0\n");
         if (strcmp(FileName[i], " ") == 0)
         {
            fprintf(stderr, "1\n");
            strippedFileName = (char*)malloc(sizeof(char) * (i));

            fprintf(stderr, "2\n");
            int k = 0;
            for (;k < i-1; k++)
            {
               fprintf(stderr, "k: %d\n", k);
               strippedFileName[k] = FileName[k];
            }

            strippedFileName[i-1] = '\0';

            fprintf(stderr, "3\n");
            newFile = true;
            break;
         }
      }

      if (!newFile)
      {
         strippedFileName = (char*)malloc(sizeof(char) * 9);

         int k;
         for (k = 0; k < 9; k++)
         {
            strippedFileName[k] = FileName[k];
         }

         strippedFileName[8] = '\0';
      }

      if (hasExtension)
      {
         for (i = 8; i < 12; i++)
         {
            if (strcmp(FileName[i], " ") == 0)
            {
               strippedExtension = (char*)malloc(sizeof(char) * (i));

               int k = 0;
               for (;k < i-1; k++)
               {
                  strippedExtension[k] = FileName[k + 8];
               }

               strippedExtension[i-1] = '\0';

               newExt = true;
               break;
            }
         }
      }

      if (!newExt)
      {
         strippedExtension = (char*)malloc(sizeof(char) * 5);

         int k;
         for (k = 0; k < 5; k++)
         {
            strippedExtension[k] = FileName[k + 8];
         }

         strippedExtension[4] = '\0';
      }

      fprintf(stderr, "%s\n", strippedFileName);

      if (hasExtension)
         fprintf(stderr, "%s\n", strippedExtension);
      else
         fprintf(stderr, "\n");
      fprintf(stderr, "%s\n", FileName);
      */
   }

}

int main( int argc, char **argv )
{
   int shmid = 0;
   void *mempointer = NULL;
   //find shm segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
      perror("shmget");

   //allocate shm to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   char newDir[9];


   char* floppyImageName;
   char* currentFLCStr;
   int currentFLC;

   int wordCount = 0;
   char* nextWordPtr = strtok(mempointer, "~");
   floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   floppyImageName[strlen(floppyImageName)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   FILE_PATH[strlen(FILE_PATH)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   currentFLCStr[strlen(currentFLCStr)] = '\0';

   sscanf(currentFLCStr, "%d", &currentFLC);

   FILE_SYSTEM_ID = fopen(mempointer, "r+");

   BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
   unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));

   if(argc == 1)
      printSector( boot, currentFLC);
   else
      printFile(boot, currentFLC, argv[1]);

   //deallocate shm
   if (shmdt(mempointer) < 0)
      perror("shmdt");
   return 0;
}