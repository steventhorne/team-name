#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

#define true 1;
#define false 0;

typedef int bool;
typedef unsigned char byte;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
    char name[9];
    char extension[4];
    int starting_byte;
    unsigned char attributes;
    short creation_time;
    short creation_date;
    short last_access_date;
    short last_write_time;
    short last_write_date;
    short flc;
    int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);
extern void writeSection( int startingByte, int sectorSize, unsigned char* boot, unsigned char* data);
extern unsigned char readSection(int startingByte, int sectorSize, unsigned char* boot);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void tryToDelete(unsigned char* boot)
{
    fprintf(stderr, "Formatting Floppy\n");
    int flc = 19;
    read_sector(flc, (char *)boot );
    
    
    
    char* temp;
    char FileName[13];
    
    int i,j;
    unsigned char deleted = 0xE5;
    for (j = 0; j < 16; j++)
    {
        char* strippedFileName;
        char* strippedExtension;
        bool hasExtension = false;
        bool newExt = false;
        bool newFile = false;
        
        /************************************************************
         *  File Name         Staring Byte: 0      Length: 8 Bytes   *
         *************************************************************/
        for (i = 0; i < 8; i++)
        {
            temp = readSection(j*32 + i, 1, boot);
            FileName[i] = temp;
        }
        
        int attributes = readSection(j*32 + 11, 1, boot);
        int checkAtt = 0x10;
        if (!((attributes & checkAtt) && checkAtt))
        {
            FileName[i] = '.';
            
            /************************************************************
             *  Extension         Staring Byte: 8      Length: 3 Bytes   *
             *************************************************************/
            for (i = 8; i < 11; i++)
            {
                temp = readSection(j*32 + i, 1, boot);
                FileName[i + 1] = temp;
            }
            
            hasExtension = true;
        }
        else
        {
            FileName[8] = ' ';
            FileName[9] = ' ';
            FileName[10] = ' ';
            FileName[11] = ' ';
        }
        
        FileName[12] = '\0';
        FILE_INFO info = get_file_info(FileName, boot, flc);
        
        if(info.flc == -1)
        {
            continue;
        }
        else
        {
            writeSection(info.starting_byte, 1, boot, &deleted);
            write_sector(flc, boot);
        }
        
    }
    
    unsigned char* sectorBoot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char) * 9);
    
    for (i = 1; i < 9; ++i)
        read_sector(i, sectorBoot + BYTES_PER_SECTOR * (i - 1));

    for(i = 2; i < 2847; ++i)
    {
        set_fat_entry(i, 0x00, sectorBoot);
    }
    
    for (i = 1; i < 9; ++i)
        write_sector(i, sectorBoot);

}

int main( int argc, char **argv )
{
    int shmid = 0;
    void *mempointer = NULL;
    //find shm segment
    if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
        perror("shmget");
    
    //allocate shm to address space
    if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
        perror("shmat");
    
    char newDir[9];
    
    
    char* floppyImageName;
    char* currentFLCStr;
    int currentFLC;
    
    int wordCount = 0;
    char* nextWordPtr = strtok(mempointer, "~");
    floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    floppyImageName[strlen(floppyImageName)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    FILE_PATH[strlen(FILE_PATH)] = '\0';
    
    nextWordPtr = strtok(NULL, "~");
    currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
    currentFLCStr[strlen(currentFLCStr)] = '\0';
    
    sscanf(currentFLCStr, "%d", &currentFLC);
    
    
    FILE_SYSTEM_ID = fopen(mempointer, "r+");
    BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
    unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char));
    tryToDelete(boot);
    
    char newSHM[255];
    
    /*strcpy(newSHM, "/~19");
    memcpy(mempointer, newSHM, strlen(newSHM) + 1);
    fprintf(stderr, "Test");*/
    
    strcpy(newSHM, "");
    strcat(newSHM, "/");
    strcat(newSHM, "~");
    char temp[255];
    snprintf(temp, 255, "%d", 19);
    memcpy(currentFLCStr, temp, strlen(temp) + 1);
    strcat(newSHM, currentFLCStr);
    memcpy(mempointer, newSHM, strlen(newSHM) + 1);

    //deallocate shm
    if (shmdt(mempointer) < 0)
        perror("shmdt");

    return 0;
}
