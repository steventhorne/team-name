#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

#define SHMKEY 666
#define SHMSIZE 1024
#define BYTES_TO_READ_IN_BOOT_SECTOR 512

typedef unsigned char byte;

int BYTES_PER_SECTOR;
FILE* FILE_SYSTEM_ID;

char* FILE_PATH;
char FILE_NAME[9];

typedef struct FILE_INFO_S
{
   char name[9];
   char extension[4];
   int starting_byte;
   unsigned char attributes;
   short creation_time;
   short creation_date;
   short last_access_date;
   short last_write_time;
   short last_write_date;
   short flc;
   int file_size;
} FILE_INFO;

extern int read_sector(int sector_number, char* buffer);
extern int get_fat_entry(int fat_entry_number, unsigned char* fat);
extern FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc);

void checkFreeDisk(unsigned char* boot, char* filePath, int flc)
{
   int numClusters, i;
   numClusters = 0;
   
   for(i = 0; i < 2847; ++i)
   {
      if(get_fat_entry(i, boot) == 0x00)
      {
         numClusters++;
      }
   }
   
   fprintf(stderr, "2847 total blocks      %d used      %d available      %05.2f percent used\n", 2847-numClusters, numClusters, (2847.0-(double)numClusters)/2847.0*100.0);
}

int main( int argc, char **argv )
{
   int shmid = 0;
   void *mempointer = NULL;
   //find shm segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0666)) < 0)
      perror("shmget");

   //allocate shm to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   char* floppyImageName;
   char* currentFLCStr;
   int currentFLC;

   int wordCount = 0;
   char* nextWordPtr = strtok(mempointer, "~");
   floppyImageName = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   floppyImageName[strlen(floppyImageName)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   FILE_PATH = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   FILE_PATH[strlen(FILE_PATH)] = '\0';

   nextWordPtr = strtok(NULL, "~");
   currentFLCStr = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
   currentFLCStr[strlen(currentFLCStr)] = '\0';

   sscanf(currentFLCStr, "%d", &currentFLC);

   FILE_SYSTEM_ID = fopen(mempointer, "r+");

   BYTES_PER_SECTOR = BYTES_TO_READ_IN_BOOT_SECTOR;
   unsigned char* boot = (unsigned char*) malloc(BYTES_TO_READ_IN_BOOT_SECTOR * sizeof(unsigned char) * 9);
   
   int i;
   for (i = 1; i < 9; ++i)
      read_sector(i, boot + BYTES_PER_SECTOR * (i - 1));

   checkFreeDisk(boot, argv[1], currentFLC);

   //deallocate shm
   if (shmdt(mempointer) < 0)
      perror("shmdt");
   return 0;
}