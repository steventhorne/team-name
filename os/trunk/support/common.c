#include <string.h>
#include <stdio.h>

extern int read_sector(int sector_number, char* buffer);

typedef struct FILE_INFO_S
{
   char name[9];
   char extension[4];
   int starting_byte;
   unsigned char attributes;
   short creation_time;
   short creation_date;
   short last_access_date;
   short last_write_time;
   short last_write_date;
   short flc;
   int file_size;
} FILE_INFO;

unsigned char readSection( int startingByte, int sectorSize, unsigned char* boot)
{
   int i = 0;
   unsigned char bits = 0;

   for(i = 0; i < sectorSize; i++)
   {
      bits = bits | ((((unsigned char) boot[startingByte + i] ) << 8 * i) & 0xffffffff);

   }

   return bits;
}

void writeSection( int startingByte, int sectorSize, unsigned char* boot, unsigned char* data)
{
   int i;
   for(i = 0; i < sectorSize; ++i)
   {
      boot[startingByte + i] = data[i];
   }
}

FILE_INFO get_file_info(char* path, unsigned char* buffer, int flc)
{
   int currentFlc = flc;
   FILE_INFO info;

   char emptyExtension[4];
   emptyExtension[0] = ' ';
   emptyExtension[1] = ' ';
   emptyExtension[2] = ' ';
   emptyExtension[3] = '\0';

   char* result = NULL;
   char* extension;
   char realExtension[4];
   
   char* pleaseWorkSavedThingy;
   
   if (path[0] == '/')
      currentFlc = 19;
   
   result = strtok_r( path, "/" , &pleaseWorkSavedThingy);

   
   while( result != NULL )
   {
      int i, j, realLen;
      char realResult[9];

      char dotCheck[9];
      
      if (strlen(result) > 8)
         realLen = 8;
      else
         realLen = strlen(result);
      
      for(i = 0; i < realLen; ++i)
      {
         dotCheck[i] = result[i];
      }
      for(i = realLen; i < 8; ++i)
      {
         dotCheck[i] = ' ';
      }
      dotCheck[8] = '\0';
      if ( strcmp(dotCheck, ".       ") == 0 || strcmp(dotCheck, "..      ") == 0)
      {
         realResult[0] = dotCheck[0];
         realResult[1] = dotCheck[1];
         realResult[2] = dotCheck[2];
         realResult[3] = dotCheck[3];
         realResult[4] = dotCheck[4];
         realResult[5] = dotCheck[5];
         realResult[6] = dotCheck[6];
         realResult[7] = dotCheck[7];
         realResult[8] = '\0';
         
         extension = NULL;
      }
      else
      {
         char* strtokSavePointer2 = NULL;
         if (strlen(result) > 8)
            realLen = 8;
         else
            realLen = strlen(result);
         
         for (i = 0; i < realLen; ++i)
         {
            realResult[i] = result[i];
            
            if(realResult[i] == '.')
            {
               realResult[i] = ' ';
               realLen = i;
            }
         }
         
         for (i = realLen; i < 8; i++)
            realResult[i] = ' ';
         
         realResult[8] = '\0';
         extension = strtok_r( result, "." , &strtokSavePointer2);
         
         if(strcmp(result, extension) == 0)
         {
            extension = strtok_r( NULL, ".", &strtokSavePointer2);
         }
         
         if(extension != NULL)
         {
            
            if (strlen(extension) > 3)
               realLen = 3;
            else
               realLen = strlen(extension);
            
            for(i = 0; i < realLen; ++i)
            {
               realExtension[i] = extension[i];
            }
            for(i = realLen; i < 3; ++i)
            {
               realExtension[i] = ' ';
            }
            realExtension[3] = '\0';
            
         }
      }
      

      read_sector(currentFlc, (char*)buffer);
      info.name[8] = '\0';

      int success = 0;
      char temp;
      for (j = 0; j < 16; j++)
      {
         for (i = 0; i < 8; i++)
         {
            temp = readSection(j*32 + i, 1, buffer);
            info.name[i] = temp;
         }
         
         if (strcasecmp(info.name, realResult) == 0)
         {
            
            for(i = 0; i < 3; ++i)
            {
               temp = readSection(j*32 + i + 8, 1, buffer);
               info.extension[i] = temp;
            }
            
            info.extension[3] = '\0';

            if(extension == NULL && strcasecmp(info.extension, emptyExtension) == 0)
            {
               //Does Exist and is a directory
               success = 1;
            }
            else if(strcasecmp(info.extension, realExtension) == 0)
            {
               
               //Does exist and is a file
               success = 1;
            }
            else
            {
               //Extension does not match input
               continue;
            }

            //Set File Info
            info.starting_byte =    j*32;
            info.attributes =       readSection(j*32 + 11, 1, buffer);
            info.creation_time =    readSection(j*32 + 14, 2, buffer);
            info.creation_date =    readSection(j*32 + 16, 2, buffer);
            info.last_access_date = readSection(j*32 + 18, 2, buffer);
            info.last_write_time =  readSection(j*32 + 22, 2, buffer);
            info.last_write_date =  readSection(j*32 + 24, 2, buffer);
            info.flc =              readSection(j*32 + 26, 2, buffer);
            info.file_size =        readSection(j*32 + 28, 4, buffer);
            
            if (info.flc != 0)
               currentFlc = info.flc + 31;
            else
               currentFlc = 19;
            
            break;
         }
      }

      if (!success)
      {
         int i, realLen;

         if (strlen(result) > 8)
            realLen = 8;
         else
            realLen = strlen(result);

         for (i = 0; i < realLen; ++i)
            info.name[i] = result[i];

         for (i = realLen; i < 8; i++)
            info.name[i] = ' ';

         info.name[8] = '\0';

         info.flc = -1;
         return info;
      }
      
      result = strtok_r( NULL, "/" , &pleaseWorkSavedThingy);
   }

   return info;
}
