solution "Team Name"
   configurations { "Debug", "Release" }

   -- A project defines one build target
   project "shell"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/shell/" )
      location ( "shell/" )
      files { "shell/**.h", "shell/**.c", "support/**.*"}
      targetname "shell"
      targetdir "../bin/"

   project "pbs"
      kind "ConsoleApp"
      language "C"
      objdir( "../obj/pbs/" )
      location ( "pbs/" )
      files { "pbs/**.c", "pbs/**.h", "support/**.*" }
      targetname "pbs"
      targetdir "../bin/commands/"

   project "pfe"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/pfe/" )
      location ( "pfe/" )
      files { "pfe/**.c", "pfe/**.h", "support/**.*" }
      targetname "pfe"
      targetdir "../bin/commands/"

   project "cd"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/cd/" )
      location ( "cd/" )
      files { "cd/**.*", "support/**.*" }
      targetname "cd"
      targetdir "../bin/commands/"

   project "pwd"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/pwd/" )
      location "pwd/"
      files { "pwd/**.*" }
      targetname "pwd"
      targetdir "../bin/commands/"

   project "ls"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/ls/" )
      location "ls/"
      files { "ls/**.*", "support/**.*" }
      targetname "ls"
      targetdir "../bin/commands/"

   project "rm"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/rm/" )
      location "rm/"
      files { "rm/**.*", "support/**.*" }
      targetname "rm"
      targetdir "../bin/commands/"

   project "rmdir"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/rmdir/" )
      location "rmdir/"
      files { "rmdir/**.*", "support/**.*" }
      targetname "rmdir"
      targetdir "../bin/commands/"

   project "touch"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/touch/" )
      location "touch/"
      files { "touch/**.*", "support/**.*" }
      targetname "touch"
      targetdir "../bin/commands/"

   project "mkdir"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/mkdir/", "support/**.*" )
      location "mkdir/"
      files { "mkdir/**.*", "support/**.*" }
      targetname "mkdir"
      targetdir "../bin/commands/"

   project "df"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/df/", "support/**.*" )
      location "df/"
      files { "df/**.*", "support/**.*" }
      targetname "df"
      targetdir "../bin/commands/"

   project "cat"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/cat/", "support/**.*" )
      location "cat/"
      files { "cat/**.*", "support/**.*" }
      targetname "cat"
      targetdir "../bin/commands/"

   project "fmt"
      kind "ConsoleApp"
      language "C"
      objdir ( "../obj/fmt/", "support/**.*" )
      location "fmt/"
      files { "fmt/**.*", "support/**.*" }
      targetname "fmt"
      targetdir "../bin/commands/"

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize" }
