/******************************************************************************
 * main: Sample for starting the FAT project.
 *
 * Authors:  Andy Kinley, Archana Chidanandan, David Mutchler and others.
 *           March, 2004.
 *****************************************************************************/

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../support/dda.h"

// 13 is NOT the correct number -- you fix it!
#define BYTES_TO_READ_IN_BOOT_SECTOR 13
#define SHMKEY 666
#define SHMSIZE 1024

/******************************************************************************
 * You must set these global variables:
 *    FILE_SYSTEM_ID -- the file id for the file system (here, the floppy disk
 *                      filesystem)
 *    BYTES_PER_SECTOR -- the number of bytes in each sector of the filesystem
 *
 * You may use these support functions (defined in FatSupport.c)
 *    read_sector
 *    write_sector
 *    get_fat_entry
 *    set_fat_entry
 *****************************************************************************/

FILE* FILE_SYSTEM_ID;
int BYTES_PER_SECTOR;

char* PATH_NAME;
char* CURRENT_FLC;

extern int read_sector(int sector_number, char* buffer);
extern int write_sector(int sector_number, char* buffer);

extern int  get_fat_entry(int fat_entry_number, char* fat);
extern void set_fat_entry(int fat_entry_number, int value, char* fat);

/******************************************************************************
 * main: an example of reading an item in the boot sector
 *****************************************************************************/

int main(int argc, char **argv) // here you can pass in the drive to mount
{
   char commandFull[255];
   int validCommand = 0;
   int numArgs = 0;

   PATH_NAME = "/";
   CURRENT_FLC = "19";

   // You must set two global variables for the disk access functions:
   //      FILE_SYSTEM_ID         BYTES_PER_SECTOR

   // Use this for the real floppy drive
   //  FILE_SYSTEM_ID = fopen(DISK_FDD0, "r+");

   int shmid = 0;
   void *mempointer = NULL;

   //create SHM segment
   if ((shmid = shmget(SHMKEY, SHMSIZE, 0666 | IPC_CREAT)) < 0)
      perror("shmget");

   //allocate SHM to address space
   if ((mempointer = shmat(shmid, NULL, 0)) == NULL)
      perror("shmat");

   if (argc <= 1)
   {
      printf("No drive to mount was specified. Defaulting to floppy1.\n");
      FILE_SYSTEM_ID = fopen( "floppy1", "r+" );
   }
   else
   {
      printf("Mounting %s.\n", argv[1]);
      FILE_SYSTEM_ID = fopen(argv[1], "r+");
   }

   if (FILE_SYSTEM_ID == NULL)
   {
      printf("Could not open the floppy drive or image.\n");
      exit(1);
   }

   //command loop
   do
   {
     char currentSHM[1000];
      fputs("> ", stdout); //prompt
      fflush(stdout); //flushes the line... sum stupid thing for no \n
      if ( fgets(commandFull, sizeof commandFull, stdin) != NULL ) // gets input
      {
         char *newline = strchr(commandFull, '\n'); // finds the \n char
         if ( newline != NULL ) // if there was a newline char
         {
            *newline = '\0'; // replace with null terminator
         }

         char *command;
         char args[255][255];

         command = strtok (commandFull, " ");


         if (command == NULL)
            continue;
      /**********************************************************************************
            PWD Command
      ***********************************************************************************/
         if (strcmp(command, "pwd") == 0)
         {
            numArgs = 0;
            validCommand = 1;

            memcpy(mempointer, PATH_NAME, strlen(PATH_NAME) + 1);
         }
      /**********************************************************************************
      *     CD Command                                                 *
      ***********************************************************************************/
         else if (strcmp(command, "cd") == 0)
         {
          //copy to SHMAT
            if (argc <= 1)
            {
            strcpy(currentSHM, "");
            strcat(currentSHM, "floppy1~");
            strcat(currentSHM, PATH_NAME);
            strcat(currentSHM, "~");
            strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }
            else
            {
            strcpy(currentSHM, "");
            strcat(currentSHM, argv[1]);
            strcat(currentSHM, "~");
            strcat(currentSHM, PATH_NAME);
            strcat(currentSHM, "~");
            strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }

            command = strtok(NULL, " ");

            if(command == NULL)
            {
               numArgs = 0;
               validCommand = 1;
            }
            else
            {
               strcpy(args[0], command);

               numArgs = 1;
               validCommand = 1;
            }

         //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
         //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
            command = strtok (commandFull, " ");
         }

      /**********************************************************************************
      *     LS Command                                                 *
      ***********************************************************************************/
         else if (strcmp(command, "ls") == 0)
         {
             //copy to SHMAT
             if (argc <= 1)
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, "floppy1~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             else
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, argv[1]);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             
             command = strtok(NULL, " ");
             
             if(command == NULL)
             {
                 numArgs = 0;
                 validCommand = 1;
             }
             else
             {
                 strcpy(args[0], command);
                 
                 numArgs = 1;
                 validCommand = 1;
             }
             
             //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
             //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
             command = strtok (commandFull, " ");
         }

     /**********************************************************************************
      *     RM Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "rm") == 0)
         {
            //copy to SHMAT
            if (argc <= 1)
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, "floppy1~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }
            else
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, argv[1]);
               strcat(currentSHM, "~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }

            command = strtok(NULL, " ");

            if(command == NULL)
            {
               numArgs = 0;
               validCommand = 1;
            }
            else
            {
               strcpy(args[0], command);

               numArgs = 1;
               validCommand = 1;
            }

            //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
            //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
            command = strtok (commandFull, " ");
         }

         /**********************************************************************************
      *     TOUCH Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "touch") == 0)
         {
            //copy to SHMAT
            if (argc <= 1)
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, "floppy1~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }
            else
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, argv[1]);
               strcat(currentSHM, "~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }

            command = strtok(NULL, " ");

            if(command == NULL)
            {
               numArgs = 0;
               validCommand = 1;
            }
            else
            {
               strcpy(args[0], command);

               numArgs = 1;
               validCommand = 1;
            }

            //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
            //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
            command = strtok (commandFull, " ");
         }

          /**********************************************************************************
           *     MKDIR Command                                                *
           ***********************************************************************************/
         else if (strcmp(command, "mkdir") == 0)
         {
             //copy to SHMAT
             if (argc <= 1)
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, "floppy1~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             else
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, argv[1]);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }

             command = strtok(NULL, " ");

             if(command == NULL)
             {
                 numArgs = 0;
                 validCommand = 1;
             }
             else
             {
                 strcpy(args[0], command);

                 numArgs = 1;
                 validCommand = 1;
             }

             //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
             //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
             command = strtok (commandFull, " ");
         }

          /**********************************************************************************
           *     RM Command                                                *
           ***********************************************************************************/
         else if (strcmp(command, "rm") == 0)
         {
             //copy to SHMAT
             if (argc <= 1)
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, "floppy1~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             else
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, argv[1]);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }

             command = strtok(NULL, " ");

             if(command == NULL)
             {
                 numArgs = 0;
                 validCommand = 1;
             }
             else
             {
                 strcpy(args[0], command);

                 numArgs = 1;
                 validCommand = 1;
             }

             //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
             //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
             command = strtok (commandFull, " ");
         }
          
          /**********************************************************************************
           *     RMDIR Command                                                *
           ***********************************************************************************/
         else if (strcmp(command, "rmdir") == 0)
         {
             //copy to SHMAT
             if (argc <= 1)
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, "floppy1~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             else
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, argv[1]);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             
             command = strtok(NULL, " ");
             
             if(command == NULL)
             {
                 numArgs = 0;
                 validCommand = 1;
             }
             else
             {
                 strcpy(args[0], command);
                 
                 numArgs = 1;
                 validCommand = 1;
             }
             
             //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
             //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
             command = strtok (commandFull, " ");
         }

         /**********************************************************************************
      *     CAT Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "df") == 0)
         {
            //copy to SHMAT
            if (argc <= 1)
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, "floppy1~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }
            else
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, argv[1]);
               strcat(currentSHM, "~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }

            command = strtok(NULL, " ");

            if(command == NULL)
            {
               numArgs = 0;
               validCommand = 1;
            }
            else
            {
               strcpy(args[0], command);

               numArgs = 1;
               validCommand = 1;
            }

            //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
            //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
            command = strtok (commandFull, " ");
         }

      /**********************************************************************************
      *     CAT Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "cat") == 0)
         {
            //copy to SHMAT
            if (argc <= 1)
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, "floppy1~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }
            else
            {
               strcpy(currentSHM, "");
               strcat(currentSHM, argv[1]);
               strcat(currentSHM, "~");
               strcat(currentSHM, PATH_NAME);
               strcat(currentSHM, "~");
               strcat(currentSHM, CURRENT_FLC);
               memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
            }

            command = strtok(NULL, " ");

            if(command == NULL)
            {
               numArgs = 0;
               validCommand = 1;
            }
            else
            {
               strcpy(args[0], command);

               numArgs = 1;
               validCommand = 1;
            }

            //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
            //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
            command = strtok (commandFull, " ");
         }

      /**********************************************************************************
      *     PFE Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "pfe") == 0)
         {
            //copy to SHMAT
            if (argc <= 1)
            {
               memcpy(mempointer, "floppy1", strlen("floppy1")+1);
            }
            else
            {
               memcpy(mempointer, argv[1], strlen(argv[1])+1);
            }

            int i;
            for ( i = 0; i < 2; i++ )
            {
               command = strtok(NULL, " ");

               if (command == NULL)
               {
                  printf("ERROR: Not enough arguments.\n");
                  break;
               }

               strcpy(args[i], command);

               //printf("arg %d: %s\n", i, args[i]);

               numArgs = 2;
               validCommand = 1;
            }

            command = strtok (commandFull, " ");
         }
      /**********************************************************************************
      *     PBS Command                                                *
      ***********************************************************************************/
         else if (strcmp(command, "pbs") == 0)
         {
          //copy to SHMAT
            if (argc <= 1)
            {
               memcpy(mempointer, "floppy1", strlen("floppy1")+1);
            }
            else
            {
               memcpy(mempointer, argv[1], strlen(argv[1])+1);
            }

            numArgs = 0;
            validCommand = 1;
         }
          /**********************************************************************************
           *     FMT Command                                                *
           ***********************************************************************************/
         else if (strcmp(command, "fmt") == 0)
         {
             //copy to SHMAT
             if (argc <= 1)
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, "floppy1~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             else
             {
                 strcpy(currentSHM, "");
                 strcat(currentSHM, argv[1]);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, PATH_NAME);
                 strcat(currentSHM, "~");
                 strcat(currentSHM, CURRENT_FLC);
                 memcpy(mempointer, currentSHM, strlen(currentSHM)+1);
             }
             
             command = strtok(NULL, " ");
             
             numArgs = 0;
             validCommand = 1;
             
             //fprintf(stderr, "File Path before CD: %s\n", PATH_NAME);
             //fprintf(stderr, "FLC before CD: %s\n", CURRENT_FLC);
             command = strtok (commandFull, " ");
         }

      /**********************************************************************************
      *     Command  Execution                                            *
      ***********************************************************************************/
         if (validCommand)
         {
            //char* data = shmat(shmid, (void *)0, 0);

            //if (data == (char*)(-1))
               //perror("shmat");

            int pid = fork();

            if (pid == 0) // i'm the child!
            {
               char commandexec[255] = "commands/";
               strcat(commandexec, command);

               switch (numArgs)
               {
                  case 0:
                     execl(commandexec, command, 0);
                     break;
                  case 1:
                     execl(commandexec, command, args[0], 0);
                     break;
                  case 2:
                     execl(commandexec, command, args[0], args[1], 0);
                     break;
                  default:
                     break;
               }


            }
            else // i'm the dad!
            {
               int status;
               while (-1 == waitpid(pid, &status, 0));

               if(strcmp(command, "cd") == 0 || strcmp(command, "fmt") == 0)
               {
                  char* nextWordPtr = strtok(mempointer, "~");
                  PATH_NAME = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
                  PATH_NAME[strlen(PATH_NAME)] = '\0';

                  nextWordPtr = strtok(NULL, "~");
                  CURRENT_FLC = nextWordPtr? strdup(nextWordPtr) : nextWordPtr;
                  CURRENT_FLC[strlen(CURRENT_FLC)] = '\0';

                  //fprintf(stderr, "File Path after CD: %s\n", PATH_NAME);
                  //fprintf(stderr, "FLC after CD: %s\n", CURRENT_FLC);
               }
            }
         }

         validCommand = 0;
      }
   } while (strcmp(commandFull, "exit") != 0);

   //deallocate SHM
   if (shmdt(mempointer) < 0)
      perror("shmdt");

   return 0;
}
